data:extend({
  {
    type = "item",
    name = "science-pump",
    order = "b-c-a",
    stack_size = 50,
    flags = { "goes-to-quickbar" },
    icon = "__liquid-science__/graphics/icons/chemical-plant.png",
    place_result = "science-pump",
    icon_size = 32
  }
})
